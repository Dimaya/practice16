
window.addEventListener('DOMContentLoaded', ()=>{
    
    //tabs

    const tabs = document.querySelectorAll('.tab__item'),
        tabsContent = document.querySelectorAll('.tabcontent'),
        tabParent = document.querySelector('.tab__items');

    function HideTabContent () {
        tabsContent.forEach(item => {
            item.classList.add ('hide')
            item.classList.remove ('show', 'fade')
        })
        tabs.forEach( item => {
            item.classList.remove('tab__item_active')
        })
    }
    function showTabContent (i = 0) {
        tabsContent[i].classList.add('show', 'fade')
        tabsContent[i].classList.remove('hide')         
        tabs[i].classList.add('tab__item_active')

    }
    HideTabContent();
    showTabContent();

    tabParent.addEventListener('click', (event) => {
         const target = event.target;

        if(target && target.classList.contains('tab__item')){
            tabs.forEach((item, i) => {
                if (target == item){
                    HideTabContent();
                    showTabContent(i);
                }
            })
        }
    })

    
    // timer
    
    const deadLine = '2022-12-11'
     
    function getTimeRemeaning (endtime){
        const t = Date.parse(endtime) - Date.parse(new Date()),
            days = Math.floor(t/(1000*60*60*24)),
            hours = Math.floor((t/(1000 *60*60) % 24)),
            minutes = Math.floor((t/1000/60) % 60),
            seconds = Math.floor((t/1000) % 60);
     
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        }
    }

    function getZero(num){
        if(num >= 0 && num < 10){   
            return `0${num}`
        } else {
            return num
        }

    }

    function setClock (selector, endtime) {
        const timer = document.querySelector(selector),
        days = timer.querySelector('#days'),
        hours = timer.querySelector('#hours'),
        minutes = timer.querySelector('#minutes'),
        seconds = timer.querySelector('#seconds'),
        timesInterval = setInterval(updateClock, 1000)

    updateClock()

        function updateClock(){
            const t = getTimeRemeaning(endtime)

            days.innerHTML = getZero(t.days)
            hours.innerHTML = getZero( t.hours)
            minutes.innerHTML = getZero(t.minutes)
            seconds.innerHTML = getZero(t.seconds)
            

            if(t.total <= 0){
                clearInterval(timesInterval);
            }
        }
    }

    setClock('.timer', deadLine)

   // modal//

    const modalTrigger = document.querySelectorAll('[data-modal]'),
        modal = document.querySelector('.modal'),
        modalCloseBtn =document.querySelector('[data-close]');

    modalTrigger.forEach(btn =>{
        btn.addEventListener('click', () =>{
        modal.classList.add('show')
        modal.classList.remove('hide')
        document.body.style.overflow = 'hidden'
        })
    })

    function closeModal (){
        modal.classList.add('hide')
        modal.classList.remove('show')
        document.body.style.overflow = ''
    }

    modalCloseBtn.addEventListener('click', () =>{
        closeModal()
    })
    modal.addEventListener('click', (e)=>{
        if(e.target === modal) {
            closeModal()
        }
    })
    document.addEventListener('keydown', (e) =>{
        if (e.code === 'Escape' && modal.classList.contains('show')){
            closeModal()
        }
    })

    //slider-jquery
    $(function () {
        $('.cars-slider').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            
        });
    });
})